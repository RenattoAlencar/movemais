package movemais.operadores.api.entity.listener;

import java.util.Date;

public interface Creatable {
	
	public Object setCreatedAt(final Date date);

}