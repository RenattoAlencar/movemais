package movemais.operadores.api.entity.listener;

import java.util.Date;

import javax.persistence.PrePersist;

public class CreatedAtListener {
	
	@PrePersist
	public void prePersist(final Creatable entity) {
		entity.setCreatedAt(new Date());
	}

}
