package movemais.operadores.api.entity.listener;

import java.util.Date;

import javax.persistence.PrePersist;

public class UpdatedAtListener {
	
	@PrePersist
	public void prePersist(final Updatable entity) {
		entity.setUpdatedAt(new Date());
	}

}