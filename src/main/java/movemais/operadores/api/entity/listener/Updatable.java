package movemais.operadores.api.entity.listener;

import java.util.Date;

public interface Updatable {

	public Object setUpdatedAt(Date date);
	
}