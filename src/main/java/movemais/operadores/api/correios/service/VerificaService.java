package movemais.operadores.api.correios.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import movemais.operadores.api.correios.payload.integracao.VerificaServicoRequest;
import movemais.operadores.api.correios.soap.client.WSClientFactory;
import movemais.operadores.api.correios.soap.wsdl.AtendeCliente;
import movemais.operadores.api.correios.soap.wsdl.AtendeClienteService;
import movemais.operadores.api.correios.soap.wsdl.AutenticacaoException;
import movemais.operadores.api.correios.soap.wsdl.SigepClienteException;

@Service
public class VerificaService {

	@Value("${AtendeClienteService.username}")
	private String usuario;

	@Value("${AtendeClienteService.password}")
	private String senha;

	@Autowired
	private WSClientFactory wsClientFactory;

	public String validar(VerificaServicoRequest input) {
		String verificaDisponibilidadeServico = null;
		try {
			AtendeCliente atendeCliente = wsClientFactory.create(AtendeClienteService.class, AtendeCliente.class);
			verificaDisponibilidadeServico = atendeCliente.verificaDisponibilidadeServico(
					Integer.parseInt(input.getCodAdministrativo()), input.getNumeroServico(), input.getCepOrigem(),
					input.getCepDestino(), usuario, senha);
		} catch (AutenticacaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SigepClienteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return verificaDisponibilidadeServico;
	}

}
