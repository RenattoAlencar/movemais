package movemais.operadores.api.correios.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import movemais.operadores.api.correios.json.JSONUtil;
import movemais.operadores.api.correios.payload.Operador;
import movemais.operadores.api.correios.payload.Operadores;


@Service
public class ListaOperadores {

	@Value("${movemais.operadores.api.correios.parceiros.data}")
	private String data;

	public List<Operador> listar() throws JsonProcessingException {
		Operadores operadores = JSONUtil.fromJSON(data, Operadores.class);
		return operadores.getData();

	}

}
