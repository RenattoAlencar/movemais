package movemais.operadores.api.correios.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import movemais.operadores.api.correios.payload.Operador;
import movemais.operadores.api.correios.payload.integracao.BuscaClienteResponse;
import movemais.operadores.api.correios.payload.integracao.VerificaServicoRequest;
import movemais.operadores.api.correios.service.CorreiosService;
import movemais.operadores.api.correios.service.ListaOperadores;
import movemais.operadores.api.correios.service.VerificaService;
import movemais.operadores.api.correios.soap.wsdl.AutenticacaoException;
import movemais.operadores.api.correios.soap.wsdl.SigepClienteException;

@Api(tags = "Operadores")
@RestController
@RequestMapping(value = "operadores", produces = {MediaType.APPLICATION_JSON_VALUE})
public class OperadoresEndpoint {
	
	@Autowired
	private CorreiosService correiosService;

	@Autowired
	private VerificaService verificaService;
	
	@Autowired
	private ListaOperadores listaOperadores;
	
	@ApiOperation(value = "Servicos dos Correios", notes = "Lista todos os servicos do correios")
	@GetMapping("{id}/servicos")
	@ApiResponses({ 
		@ApiResponse(code = 200, message = "OK"),
		@ApiResponse(code = 412, message = "Precondition Failed")
	})
	public ResponseEntity<BuscaClienteResponse> verificarServico(@PathVariable("id") String id) throws AutenticacaoException, SigepClienteException {
		BuscaClienteResponse resp = correiosService.buscar(); 
		return ResponseEntity.status(HttpStatus.OK).body(resp);
	}
	
	
	@PostMapping("disponibilidade")
	@ApiOperation(value = "Disponibilidade Correios", notes = "Verifica a Disponibiliadade do Servico dos Correios para a regiao")
	public ResponseEntity<String> disponibilidade(@RequestBody VerificaServicoRequest input){
		String resposta = verificaService.validar(input);
		return ResponseEntity.status(HttpStatus.OK).body(resposta);
	}
	
	@GetMapping()
	@ApiOperation(value = "Listar operadores", notes = "Lista os Operadores logisticos")
	public ResponseEntity<List<Operador>> buscarOperadores() throws JsonProcessingException  {
		return ResponseEntity.ok( listaOperadores.listar());
	}
	
}
