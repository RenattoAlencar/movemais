package movemais.operadores.api.correios.payload.integracao;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.With;

@With
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class VerificaServicoRequest {
	
	@ApiModelProperty(example="17000190")
	private String codAdministrativo;
	
	@ApiModelProperty(example="04162")
	private String numeroServico;
	
	@ApiModelProperty(example="06764030")
	private String cepOrigem;
	
	@ApiModelProperty(example="02982035")
	private String cepDestino;
	
}
