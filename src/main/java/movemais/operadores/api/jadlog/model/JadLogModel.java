package movemais.operadores.api.jadlog.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Entity
@Table(name = "TB_JADLOG")
public class JadLogModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "data")
	private LocalDateTime data;

	@Column(name = "conteudo")
	private String conteudo;

	@ElementCollection
	@Column(name = "pedido")
	private List<String> pedido;

	@Column(name = "totPeso")
	private Double totPeso;

	@Column(name = "totValor")
	private Double totValor;

	@Column(name = "obs")
	private String obs;

	@Column(name = "modalidade")
	private Integer modalidade;

	@Column(name = "contaCorrente")
	private String contaCorrente;

	@Column(name = "tpColeta")
	private String tpColeta;

	@Column(name = "tipoFrete")
	private Integer tipoFrete;

	@Column(name = "cdUnidadeOri")
	private String cdUnidadeOri;

	@Column(name = "cdUnidadeDes")
	private String cdUnidadeDes;

	@Column(name = "cdPickupOri")
	private String cdPickupOri;

	@Column(name = "cdPickupDes")
	private String cdPickupDes;

	@Column(name = "nrContrato")
	private Integer nrContrato;

	@Column(name = "servico")
	private Integer servico;

	@Column(name = "shipmentId")
	private String shipmentId;

	@Column(name = "vlColeta")
	private Double vlColeta;

	@Column(name = "nome")
	private String nome;

	@Column(name = "cnpjCpf")
	private String cnpjCpf;

	@Column(name = "ie")
	private String ie;

	@Column(name = "endereco")
	private String endereco;

	@Column(name = "numero")
	private String numero;

	@Column(name = "compl")
	private String compl;

	@Column(name = "bairro")
	private String bairro;

	@Column(name = "cidade")
	private String cidade;

	@Column(name = "uf")
	private String uf;

	@Column(name = "cep")
	private String cep;

	@Column(name = "fone")
	private String fone;

	@Column(name = "cel")
	private String cel;

	@Column(name = "email")
	private String email;

	@Column(name = "contato")
	private String contato;

	@Column(name = "cfop")
	private String cfop;

	@Column(name = "danfeCte")
	private String danfeCte;

	@Column(name = "nrDoc")
	private String nrDoc;

	@Column(name = "serie")
	private String serie;

	@Column(name = "tpDocumento")
	private Integer tpDocumento;

	@Column(name = "valor")
	private Double valor;

	@Column(name = "altura")
	private Integer altura;

	@Column(name = "comprimento")
	private Integer comprimento;

	@Column(name = "identificador")
	private String identificador;

	@Column(name = "largura")
	private Integer largura;

	@Column(name = "peso")
	private Double peso;

	@Column(name = "codigo")
	private String codigo;

	@Column(name = "status")
	private String status;

}
