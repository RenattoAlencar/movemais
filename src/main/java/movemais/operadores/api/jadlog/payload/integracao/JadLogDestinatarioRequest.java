package movemais.operadores.api.jadlog.payload.integracao;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class JadLogDestinatarioRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonAlias(value = "nome")
	private String nome;
	
	@JsonAlias(value = "cnpjCpf")	
	private String cnpjCpf;
	
	@JsonAlias(value = "ie")
	private String ie;

	@JsonAlias(value = "endereco")
	private String endereco;
	
	@JsonAlias(value = "numero")
	private String numero;
	
	@JsonAlias(value = "compl")
	private String compl;
	
	@JsonAlias(value = "bairro")
	private String bairro;
	
	@JsonAlias(value = "cidade")
	private String cidade;
	
	@JsonAlias(value = "uf")
	private String uf;

	@JsonAlias(value = "cep")
	private String cep;
	
	@JsonAlias(value = "fone")
	private String fone;
	
	@JsonAlias(value = "email")
	private String email;
	
	@JsonAlias(value = "contato")
	private String contato;
}
