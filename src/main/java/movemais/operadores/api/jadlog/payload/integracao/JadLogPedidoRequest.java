package movemais.operadores.api.jadlog.payload.integracao;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class JadLogPedidoRequest implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@JsonAlias(value = "")
	private String conteudo;
	
	@JsonAlias(value = "pedido")
	private List<String> pedido;
	
	@JsonAlias(value = "totPeso")
	private Double totPeso;
	
	@JsonAlias(value = "totValor")
	private Double totValor;
	
	@JsonAlias(value = "obs")
	private String obs;
	
	@JsonAlias(value = "modalidade")
	private Integer modalidade;
	
	@JsonAlias(value = "contaCorrente")
	private String contaCorrente;
	
	@JsonAlias(value = "tpColeta")
	private String tpColeta;
	
	@JsonAlias(value = "tipoFrete")
	private Integer tipoFrete;
	
	@JsonAlias(value = "cdUnidadeOri")
	private String cdUnidadeOri;
	
	@JsonAlias(value = "cdUnidadeDes")
	private String cdUnidadeDes;
	
	@JsonAlias(value = "cdPickupOri")
	private String cdPickupOri;
	
	@JsonAlias(value = "cdPickupDes")
	private String cdPickupDes;
	
	@JsonAlias(value = "nrContrato")
	private Integer nrContrato;

	@JsonAlias(value = "servico")
	private Integer servico;

	@JsonAlias(value = "shipmentId")
	private String shipmentId;

	@JsonAlias(value = "vlColeta")
	private Double vlColeta;

	@JsonAlias(value = "dfe")
	private List<JadLogDfeRequest> dfe;
	
	@JsonAlias(value = "volume")
	private List<JadLogVolumeRequest> volume;
	
	@JsonAlias(value = "rem")
	private JadLogRemetenteRequest rem;
	
	@JsonAlias(value = "des")
	private JadLogDestinatarioRequest des;

}

