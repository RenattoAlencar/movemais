package movemais.operadores.api.jadlog.payload.integracao;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;


@With
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder(toBuilder = true)
public class Consulta implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonAlias(value = "codigo")
	private String codigo;
	private Tracking tracking;
}
