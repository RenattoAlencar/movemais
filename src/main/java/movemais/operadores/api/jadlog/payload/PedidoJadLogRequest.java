package movemais.operadores.api.jadlog.payload;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class PedidoJadLogRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull @NotEmpty
	@ApiModelProperty(example="Pendrive")
	private String conteudo;
	
	@NotNull @NotEmpty
	@JsonProperty("pedido")
	@ApiModelProperty(example="Pendrive")
	private List<String> pedido;
 
	@NotNull  
	@ApiModelProperty(example="1")
	private Double totPeso;
	
	@NotNull 
	@ApiModelProperty(example="25.76")
	private Double totValor;
	
	@NotNull @NotEmpty
	@ApiModelProperty(example="PRODUTO NOVO")
	private String obs;

	@NotNull 
	@ApiModelProperty(example="3")
	private Integer modalidade;
	
	@NotNull @NotEmpty
	@ApiModelProperty(example="0005001545")
	private String contaCorrente;

	@NotNull @NotEmpty
	@ApiModelProperty(example="K")
	private String tpColeta;

	@NotNull 
	@ApiModelProperty(example="0")
	private Integer tipoFrete;

	@NotNull @NotEmpty
	@ApiModelProperty(example="1")
	private String cdUnidadeOri;

	@NotNull @NotEmpty
	@ApiModelProperty(example="BR00001")
	private String cdUnidadeDes;

	//Alguma validação da API
	@ApiModelProperty(example="2")
	private String cdPickupOri;
	
	@ApiModelProperty(example="BR00002")
	private String cdPickupDes;
	
	@NotNull  
	@ApiModelProperty(example="")
	private Integer nrContrato;
	
	@NotNull 
	@ApiModelProperty(example="1")
	private Integer servico;
	
	@NotNull 
	@ApiModelProperty(hidden=true) //tirei para teste
	private Double vlColeta;
	
	@NotNull @NotEmpty
	@ApiModelProperty(example="DEVIRES")
	private String nome;
	
	@NotNull @NotEmpty
	@ApiModelProperty(example="03948341")
	private String cnpjCpf;
	
	@NotNull @NotEmpty
	@ApiModelProperty(hidden=true) //tirei para teste
	private String ie;
	
	@NotNull @NotEmpty
	@ApiModelProperty(example="Rua Roque Petroni")
	private String endereco;
	
	@NotNull @NotEmpty
	@ApiModelProperty(example="850")
	private String numero;
	
	@NotNull @NotEmpty
	@ApiModelProperty(example="Torre Jaceru")
	private String compl;
	
	@NotNull @NotEmpty
	@ApiModelProperty(example="Jardim das Acacias")
	private String bairro;
	
	@NotNull @NotEmpty
	@ApiModelProperty(example="Sao Paulo")
	private String cidade;
	
	@NotNull @NotEmpty
	@ApiModelProperty(example="SP")
	private String uf;
	
	@NotNull @NotEmpty
	@ApiModelProperty(example="04701000")
	private String cep;
	
	@NotNull @NotEmpty
	@ApiModelProperty(example="1199999999")
	private String fone;
	
	@NotNull @NotEmpty
	@ApiModelProperty(example="11999999999")
	private String cel;
	
	@NotNull @NotEmpty
	@ApiModelProperty(example="devires@devires.com.br")
	private String email;
	
	@NotNull @NotEmpty
	@ApiModelProperty(example="Devires")
	private String contato;
	
	@NotNull @NotEmpty
	@ApiModelProperty(example="6909")
	private String cfop;
	
	@NotNull @NotEmpty
	@ApiModelProperty(example="0000001")
	private String danfeCte;
	
	@NotNull @NotEmpty
	@ApiModelProperty(example="00001")
	private String nrDoc;
	
	@NotNull @NotEmpty
	@ApiModelProperty(example="01")
	private String serie;
	
	@NotNull 
	@ApiModelProperty(example="2")
	private Integer tpDocumento;
	
	@NotNull 
	@ApiModelProperty(example="20.8")
	private Double valor;
	
	@NotNull 
	@ApiModelProperty(example="10")
	private Integer altura;
	
	@NotNull 
	@ApiModelProperty(example="10")
	private Integer comprimento;
	
	@NotNull @NotEmpty
	@ApiModelProperty(example="123487")
	private String identificador;
	
	@NotNull 
	@ApiModelProperty(example="10")
	private Integer largura;
	
	@NotNull 
	@ApiModelProperty(example="4")
	private Double peso;

	
}
