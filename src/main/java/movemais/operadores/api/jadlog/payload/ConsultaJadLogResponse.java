package movemais.operadores.api.jadlog.payload;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;
import movemais.operadores.api.jadlog.payload.integracao.Consulta;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class ConsultaJadLogResponse implements Serializable{

	private static final long serialVersionUID = 1L;
	
		private List<Consulta> consulta;	
	
}
