package movemais.operadores.api.jadlog.payload.integracao;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class JadLogDfeRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@JsonAlias(value = "danfeCte")	
	private String danfeCte;
	
	@JsonAlias(value = "nrDoc")
	private String nrDoc;
	
	@JsonAlias(value = "serie")
	private String serie;
	
	@JsonAlias(value = "valor")
	private Double valor;

	@JsonAlias(value = "cfop")
	private String cfop;

	@JsonAlias(value = "tpDocumento")
	private Integer tpDocumento;
}
