package movemais.operadores.api.jadlog.payload.integracao;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder(toBuilder = true)
public class JadLogConsultaRequest {

	private List<Consulta> consulta;

}
