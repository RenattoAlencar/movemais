package movemais.operadores.api.jadlog.payload.integracao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder(toBuilder = true)
public class JadLogPedidoErro {

	private String descricao;
	private String id;
	
}
