package movemais.operadores.api.jadlog.payload.integracao;

import java.time.LocalDate;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class Tracking {
	private String codigo;
	private String shipmentId;
	private String dacte;
	private LocalDate dtEmissao;
	private String status;
	private Double valor;
	private Double peso;
	private List<Eventos> eventos;
	private List<Volume> volumes;

}
