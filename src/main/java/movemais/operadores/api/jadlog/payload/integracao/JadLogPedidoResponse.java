package movemais.operadores.api.jadlog.payload.integracao;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class JadLogPedidoResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	private String codigo;
	private String shipmentId;
	private String status;
	
	private JadLogPedidoErro erro;
}
