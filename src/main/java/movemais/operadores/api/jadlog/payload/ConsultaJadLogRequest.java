package movemais.operadores.api.jadlog.payload;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;


@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class ConsultaJadLogRequest implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@NotNull @NotEmpty
	@ApiModelProperty(example="012234567891")
	private String codigo;


}
