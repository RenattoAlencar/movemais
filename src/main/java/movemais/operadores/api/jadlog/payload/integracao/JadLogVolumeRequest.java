package movemais.operadores.api.jadlog.payload.integracao;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class JadLogVolumeRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonAlias(value = "altura")
	private Integer altura;

	@JsonAlias(value = "comprimento")
	private Integer comprimento;

	@JsonAlias(value = "largura")
	private Integer largura;

	@JsonAlias(value = "peso")
	private Double peso;

	@JsonAlias(value = "identificador")
	private String identificador;

}
