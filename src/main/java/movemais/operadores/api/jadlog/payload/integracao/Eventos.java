package movemais.operadores.api.jadlog.payload.integracao;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class Eventos {
	
	private LocalDate data;
	private String status;
	private String unidade;

}
