package movemais.operadores.api.jadlog.payload.integracao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class Volume {
	
	private Double peso;
	private Double altura;
	private Double largura;
	private Double comprimento;

}
