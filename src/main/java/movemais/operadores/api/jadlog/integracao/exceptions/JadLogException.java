package movemais.operadores.api.jadlog.integracao.exceptions;

public class JadLogException extends Exception{

	public JadLogException(String descricao) {
		super(descricao);
	}

	private static final long serialVersionUID = 1L;

}
