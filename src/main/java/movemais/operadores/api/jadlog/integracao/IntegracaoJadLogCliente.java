package movemais.operadores.api.jadlog.integracao;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;
import movemais.operadores.api.jadlog.integracao.exceptions.JadLogException;
import movemais.operadores.api.jadlog.payload.integracao.JadLogConsultaRequest;
import movemais.operadores.api.jadlog.payload.integracao.JadLogConsultaResponse;
import movemais.operadores.api.jadlog.payload.integracao.JadLogPedidoRequest;
import movemais.operadores.api.jadlog.payload.integracao.JadLogPedidoResponse;
import movemais.operadores.api.jadlog.payload.integracao.JadLogRemetenteRequest;

@Slf4j
@Component
public class IntegracaoJadLogCliente {

	@Value("${movemais.operadores.api.jadlog.integracao.token}")
	private String token;

	@Value("${movemais.operadores.api.jadlog.integracao.endpoint}")
	private String ENDPOINT;

	@Value("${movemais.operadores.api.jadlog.integracao.endpointtracking}")
	private String ENDPOINT_TRACKING;

	@Value("${movemais.operadores.jadlog.nome}")
	private String nome;

	@Value("${movemais.operadores.jadlog.cnpjCpf}")
	private String cnpjCpf;

	@Value("${movemais.operadores.jadlog.ie}")
	private String ie;

	@Value("${movemais.operadores.jadlog.endereco}")
	private String endereco;

	@Value("${movemais.operadores.jadlog.numero}")
	private String numero;

	@Value("${movemais.operadores.jadlog.compl}")
	private String compl;

	@Value("${movemais.operadores.jadlog.bairro}")
	private String bairro;

	@Value("${movemais.operadores.jadlog.cidade}")
	private String cidade;

	@Value("${movemais.operadores.jadlog.uf}")
	private String uf;

	@Value("${movemais.operadores.jadlog.cep}")
	private String cep;

	@Value("${movemais.operadores.jadlog.fone}")
	private String fone;

	@Value("${movemais.operadores.jadlog.email}")
	private String email;

	@Value("${movemais.operadores.jadlog.contato}")
	private String contato;

	private RestTemplate restTemplate;

	@PostConstruct
	public void init() {
		restTemplate = new RestTemplateBuilder().additionalInterceptors(HeaderToken.builder().token(token).build())
				.build();
	}

	public JadLogPedidoResponse run(JadLogPedidoRequest input) throws JadLogException {
		input = input.toBuilder()
				.rem(JadLogRemetenteRequest.builder().nome(nome).cnpjCpf(cnpjCpf).ie(ie).endereco(endereco)
						.numero(numero).compl(compl).bairro(bairro).cidade(cidade).uf(uf).cep(cep).fone(fone)
						.email(email).contato(contato).build())
				.build();

		try {
			String path = String.format("%s/incluir", ENDPOINT);
			HttpEntity<JadLogPedidoRequest> request = new HttpEntity<JadLogPedidoRequest>(input);
			ResponseEntity<JadLogPedidoResponse> response = restTemplate.postForEntity(path, request,
					JadLogPedidoResponse.class);
			if (response.getBody().getErro() == null) {
				return response.getBody();
			}else {
				throw new  JadLogException(response.getBody().getErro().getDescricao());
			}
				
			// log.info("Response: {}", response);

		} catch (HttpClientErrorException e) {
			log.error("ERROR:{}", e.getResponseBodyAsString());
			throw e;
		} catch (HttpServerErrorException e) {
			log.error("ERROR:{}", e.getResponseBodyAsString());
			throw e;
		}
	}

	public JadLogConsultaResponse run(JadLogConsultaRequest input) {
		try {
			String path = String.format("%s/consultar", ENDPOINT_TRACKING);
			HttpEntity<JadLogConsultaRequest> request = new HttpEntity<JadLogConsultaRequest>(input);
			ResponseEntity<JadLogConsultaResponse> response = restTemplate.postForEntity(path, request,
					JadLogConsultaResponse.class);
			return response.getBody();

		} catch (HttpClientErrorException e) {
			log.error("ERROR:{}", e.getResponseBodyAsString());
			throw e;
		} catch (HttpServerErrorException e) {
			log.error("ERROR:{}", e.getResponseBodyAsString());
			throw e;
		}
	}

}
