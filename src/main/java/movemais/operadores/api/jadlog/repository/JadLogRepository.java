package movemais.operadores.api.jadlog.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import movemais.operadores.api.jadlog.model.JadLogModel;

public interface JadLogRepository extends JpaRepository<JadLogModel, Long> {


}
