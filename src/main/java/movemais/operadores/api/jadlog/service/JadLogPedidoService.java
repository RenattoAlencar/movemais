package movemais.operadores.api.jadlog.service;

import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import movemais.operadores.api.jadlog.integracao.IntegracaoJadLogCliente;
import movemais.operadores.api.jadlog.integracao.exceptions.JadLogException;
import movemais.operadores.api.jadlog.model.JadLogModel;
import movemais.operadores.api.jadlog.payload.PedidoJadLogRequest;
import movemais.operadores.api.jadlog.payload.PedidoJadLogResponse;
import movemais.operadores.api.jadlog.payload.integracao.JadLogDestinatarioRequest;
import movemais.operadores.api.jadlog.payload.integracao.JadLogDfeRequest;
import movemais.operadores.api.jadlog.payload.integracao.JadLogPedidoRequest;
import movemais.operadores.api.jadlog.payload.integracao.JadLogPedidoResponse;
import movemais.operadores.api.jadlog.payload.integracao.JadLogVolumeRequest;
import movemais.operadores.api.jadlog.repository.JadLogRepository;
import movemais.operadores.api.pedidos.etiquetas.EmitirEtiquetas;
import movemais.operadores.api.postagem.payload.Endereco;
import movemais.operadores.api.postagem.payload.PostagemRequest;
import movemais.operadores.api.postagem.payload.Telefone;
import movemais.operadores.api.utils.NumerosUtils;

@Service
public class JadLogPedidoService {

	@Autowired
	private IntegracaoJadLogCliente integracaoJadLogCliente;
	
	@Autowired
	private JadLogRepository jadLogRepository;
	
	public PedidoJadLogResponse run(PostagemRequest input) throws JadLogException {
		JadLogPedidoRequest request = transform(input);
		JadLogPedidoResponse response = integracaoJadLogCliente.run(request);
		return transform(response);
	}
	
	private JadLogPedidoRequest transform(PostagemRequest input) {
	
		Telefone telefone = input.getDestinatario().getTelefone();
		Endereco endereco = input.getDestinatario().getEndereço();
		
		String numeroTelefone = null;
		
		if(telefone != null) {
			numeroTelefone = StringUtils.join(input.getDestinatario().getTelefone().getDdd(), input.getDestinatario().getTelefone().getNumero()); 
		}
		
		return JadLogPedidoRequest.builder()
				.pedido(Arrays.asList(input.getId().toString()))
				.des(JadLogDestinatarioRequest.builder()
						.nome(input.getDestinatario().getNome())
						.cnpjCpf(input.getDestinatario().getDocumento())
						.ie(input.getDadosAdicionais().get("ie"))
						.endereco(endereco.getRua())
						.numero(endereco.getNumero())
						.compl(endereco.getComplemento())
						.bairro(endereco.getBairro())
						.cidade(endereco.getCidade())
						.uf(endereco.getEstado())
						.cep(endereco.getCep())
						.fone(numeroTelefone)
						.email(input.getDestinatario().getEmail())
						.contato(input.getDestinatario().getNome())
						.build())
				.conteudo(input.getDadosAdicionais().get("conteudo"))
				.obs(input.getDadosAdicionais().get("obs"))
				.contaCorrente(input.getDadosAdicionais().get("contaCorrente"))
				.tpColeta(input.getDadosAdicionais().get("tpColeta"))
				.cdUnidadeOri(input.getDadosAdicionais().get("cdUnidadeOri"))
				.cdUnidadeDes(input.getDadosAdicionais().get("cdUnidadeDes"))
				.cdPickupOri(input.getDadosAdicionais().get("cdPickupOri"))
				.cdPickupDes(input.getDadosAdicionais().get("cdPickupDes"))
				.modalidade(NumerosUtils.parseInt(input.getDadosAdicionais().get("modalidade")))
				.tipoFrete(NumerosUtils.parseInt(input.getDadosAdicionais().get("tipoFrete")))
				.nrContrato(NumerosUtils.parseInt(input.getDadosAdicionais().get("nrContrato")))
				.servico(NumerosUtils.parseInt(input.getDadosAdicionais().get("servico")))
				.totPeso(NumerosUtils.parseDouble(input.getDadosAdicionais().get("totPeso")))
				.totValor(NumerosUtils.parseDouble(input.getDadosAdicionais().get("totValor")))
				.vlColeta(NumerosUtils.parseDouble(input.getDadosAdicionais().get("vlColeta")))
				.dfe(Arrays.asList(JadLogDfeRequest.builder()
						.danfeCte(input.getDadosAdicionais().get("danfeCte"))
						.nrDoc(input.getDadosAdicionais().get("nrDoc"))
						.serie(input.getDadosAdicionais().get("serie"))
						.cfop(input.getDadosAdicionais().get("cfop"))
						.tpDocumento(NumerosUtils.parseInt(input.getDadosAdicionais().get("tpDocumento")))
						.valor(NumerosUtils.parseDouble(input.getDadosAdicionais().get("valor")))
						.build()))
				.volume(Arrays.asList(JadLogVolumeRequest.builder()
						.identificador(input.getDadosAdicionais().get("identificador"))
						.comprimento(NumerosUtils.parseInt(input.getDadosAdicionais().get("comprimento")))
						.altura(NumerosUtils.parseInt(input.getDadosAdicionais().get("altura")))
						.largura(NumerosUtils.parseInt(input.getDadosAdicionais().get("largura")))
						.peso(NumerosUtils.parseDouble(input.getDadosAdicionais().get("peso")))
						.build()))
				.build();
	}

	public PedidoJadLogResponse jadlog(PedidoJadLogRequest input) throws JadLogException {
		JadLogPedidoRequest request = transform(input);
		JadLogPedidoResponse response = integracaoJadLogCliente.run(request);
		//TODO: AQUI NANCO DE DADOS
		mapearJadLog(input, response);
		EmitirEtiquetas emitir = new EmitirEtiquetas();
		emitir.createPdfReport(input);
		
		return transform(response);
	}

	private JadLogModel mapearJadLog(PedidoJadLogRequest input,JadLogPedidoResponse response ) {
		JadLogModel model = JadLogModel.builder()
				.conteudo(input.getConteudo())
				.pedido(input.getPedido())
				.totPeso(input.getTotPeso())
				.totValor(input.getTotValor())
				.obs(input.getObs())
				.modalidade(input.getModalidade())
				.contaCorrente(input.getContaCorrente())
				.tpColeta(input.getTpColeta())
				.tipoFrete(input.getTipoFrete())
				.cdUnidadeOri(input.getCdUnidadeOri())
				.cdUnidadeDes(input.getCdUnidadeDes())
				.cdPickupOri(input.getCdPickupOri())
				.cdPickupDes(input.getCdPickupDes())
				.nrContrato(input.getNrContrato())
				.servico(input.getServico())
				.vlColeta(input.getVlColeta())
				.nome(input.getNome())
				.cnpjCpf(input.getCnpjCpf())
				.ie(input.getIe())
				.endereco(input.getEndereco())
				.numero(input.getNumero())
				.compl(input.getCompl())
				.bairro(input.getBairro())
				.cidade(input.getCidade())
				.uf(input.getUf())
				.cep(input.getCep())
				.fone(input.getFone())
				.cel(input.getCel())
				.email(input.getEmail())
				.contato(input.getContato())
				.cfop(input.getCfop())
				.danfeCte(input.getDanfeCte())
				.nrDoc(input.getNrDoc())
				.serie(input.getSerie())
				.tpDocumento(input.getTpDocumento())
				.valor(input.getValor())
				.altura(input.getAltura())
				.comprimento(input.getComprimento())
				.identificador(input.getIdentificador())
				.largura(input.getLargura())
				.peso(input.getPeso())
				.codigo(response.getCodigo())
				.shipmentId(response.getShipmentId())
				.status(response.getStatus())
				
				.build();
			return	jadLogRepository.save(model);
		 
	}
	
	private JadLogPedidoRequest transform(PedidoJadLogRequest input) {
		return JadLogPedidoRequest.builder()
				.conteudo(input.getConteudo())
				.pedido(input.getPedido())
				.totPeso(input.getTotPeso())
				.totValor(input.getTotValor())
				.obs(input.getObs())
				.modalidade(input.getModalidade())
				.contaCorrente(input.getContaCorrente())
				.tpColeta(input.getTpColeta())
				.tipoFrete(input.getTipoFrete())
				.cdUnidadeOri(input.getCdUnidadeOri())
				.cdUnidadeDes(input.getCdUnidadeDes())
				.cdPickupOri(input.getCdPickupOri())
				.cdPickupDes(input.getCdPickupDes())
				.nrContrato(input.getNrContrato())
				.servico(input.getServico())
				//.shipmentId(input.getShipmentId())
				.vlColeta(input.getVlColeta())
				.des(JadLogDestinatarioRequest.builder()
						.nome(input.getNome())
						.cnpjCpf(input.getCnpjCpf())
						.ie(input.getIe())
						.endereco(input.getEndereco())
						.numero(input.getNumero())
						.compl(input.getCompl())
						.bairro(input.getBairro())
						.cidade(input.getCidade())
						.uf(input.getUf())
						.cep(input.getCep())
						.fone(input.getFone())
						.email(input.getEmail())
						.contato(input.getContato())
						.build())
					.dfe(Arrays.asList(JadLogDfeRequest.builder()
							.danfeCte(input.getDanfeCte())
							.nrDoc(input.getNrDoc())
							.serie(input.getSerie())
							.valor(input.getValor())
							.cfop(input.getCfop())
							.tpDocumento(input.getTpDocumento())
							.build()))
					.volume(Arrays.asList(JadLogVolumeRequest.builder()
							.altura(input.getAltura())
							.comprimento(input.getComprimento())
							.largura(input.getLargura())
							.peso(input.getPeso())
							.identificador(input.getIdentificador())
							.build()))
					.build();
	}

	private PedidoJadLogResponse transform(JadLogPedidoResponse response) {
		return PedidoJadLogResponse.builder()
				.codigo(response.getCodigo())
				.shipmentId(response.getShipmentId())
				.status(response.getStatus())
				.build();
	
	}

}
