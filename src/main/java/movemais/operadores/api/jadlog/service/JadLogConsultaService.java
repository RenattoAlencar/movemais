package movemais.operadores.api.jadlog.service;

import java.util.Arrays;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import movemais.operadores.api.jadlog.integracao.IntegracaoJadLogCliente;
import movemais.operadores.api.jadlog.payload.ConsultaJadLogRequest;
import movemais.operadores.api.jadlog.payload.ConsultaJadLogResponse;
import movemais.operadores.api.jadlog.payload.integracao.Consulta;
import movemais.operadores.api.jadlog.payload.integracao.JadLogConsultaRequest;
import movemais.operadores.api.jadlog.payload.integracao.JadLogConsultaResponse;
import movemais.operadores.api.jadlog.payload.integracao.Tracking;

@Service
public class JadLogConsultaService {

	@Autowired
	private IntegracaoJadLogCliente integracaoJadLogCliente;
	
	
	public ConsultaJadLogResponse jadlog(ConsultaJadLogRequest input) {
		JadLogConsultaRequest request = transform(input);
		JadLogConsultaResponse response = integracaoJadLogCliente.run(request);
		ConsultaJadLogResponse output = tranform(response);
		return output;
		
	}

	private JadLogConsultaRequest transform(ConsultaJadLogRequest input) {
		return JadLogConsultaRequest.builder()
				.consulta(Arrays.asList(Consulta.builder()
						.codigo(input.getCodigo())
						.build()))
				
				.build();
	}
	
	//TRACKING SÂO INFORMAÇÔES QUE NÂO TEM NA DOCUMENTAÇÂO
	private ConsultaJadLogResponse tranform(JadLogConsultaResponse response) {
		if(response != null && !CollectionUtils.isEmpty(response.getConsulta())) {
			Optional<Consulta> consulta = response.getConsulta().stream().findFirst();
			if (consulta.isPresent()) {
				return ConsultaJadLogResponse.builder()
						.consulta(Arrays.asList(Consulta.builder()
								.codigo(consulta.get().getCodigo())
								.tracking(Tracking.builder()
										.codigo(consulta.get().getTracking().getCodigo())
										.build())
								.build()))
						
						.build();
			}
		
		}
		throw new RuntimeException("Codigo não encontrado");
	}

	
}
