package movemais.operadores.api.config;

import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@Controller
@Configuration
public class WebConfig {

	@Bean
	public Jackson2ObjectMapperBuilderCustomizer customJson()
	{
		return builder -> {
			builder
			.indentOutput(true)
			.serializationInclusion(JsonInclude.Include.NON_NULL)
			.modules(new JavaTimeModule())
			.featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS,
					SerializationFeature.FAIL_ON_EMPTY_BEANS);
		};
	}
	
}