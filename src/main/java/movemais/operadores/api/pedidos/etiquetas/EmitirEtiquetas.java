package movemais.operadores.api.pedidos.etiquetas;

import java.io.File;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import movemais.operadores.api.jadlog.model.JadLogModel;
import movemais.operadores.api.jadlog.payload.PedidoJadLogRequest;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

public class EmitirEtiquetas {
	
	public void createPdfReport(PedidoJadLogRequest input) {
		
		try {
		InputStream is = JadLogModel.class.getClassLoader().getResourceAsStream("etiquetas/etiquetaJadlog.jrxml");
		JasperReport jasperReport = JasperCompileManager.compileReport(is);
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("DATE", new Date());
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters);
			File outDir = new File("D:/jasperReports");
			outDir.mkdirs();
			JasperExportManager.exportReportToPdfFile(jasperPrint, "D:/jasperReports/etiquetaJadlog.pdf");
			System.out.println("PDF report done!");
		
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
