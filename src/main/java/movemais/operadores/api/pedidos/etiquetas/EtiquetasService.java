package movemais.operadores.api.pedidos.etiquetas;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import movemais.operadores.api.jadlog.model.JadLogModel;
import movemais.operadores.api.jadlog.repository.JadLogRepository;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class EtiquetasService {

	@Autowired
	private JadLogRepository jadLogRepository;
	
	public String exportar(String reportFormat ) throws FileNotFoundException, JRException {
			List<JadLogModel> jadLogModel = jadLogRepository.findAll();
		File file = ResourceUtils.getFile("classpath:etiquetas/etiquetaJadlog.jrxml");
		JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
		JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(jadLogModel);
		Map<String, Object>parameters = new HashMap<>();
		parameters.put("createdBy", "Teste");
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
		
		JasperExportManager.exportReportToPdfFile(jasperPrint, "D:/jasperReports/etiquetaJadlog.pdf");
		return "gerado em...";
		
	}
	
}
