package movemais.operadores.api.pedidos.model;

public enum Transportadora {
	
	CORREIOS, TRANSLEVELOG, JADLOG
	
}