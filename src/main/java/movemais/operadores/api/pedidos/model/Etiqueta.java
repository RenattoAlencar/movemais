package movemais.operadores.api.pedidos.model;

import lombok.Data;

@Data
public class Etiqueta {

	private String nome;
	private String cpfCnpj;
	private String endereco;
	private String bairro;
	private String numero;
	private String cidade;
	private String estado;
	private String cep;
	private String telefone;
	private String celular;
	private String email;
	private String contato;

}
