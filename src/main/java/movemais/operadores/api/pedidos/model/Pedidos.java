package movemais.operadores.api.pedidos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;

@Getter
@Entity
@Table(name = "TB_PEDIDO")
public class Pedidos {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@Column(name = "exteroId", nullable = true)
	private String exteroId;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "transport",nullable = false)
	private Transportadora transportadora;
	
	
}
