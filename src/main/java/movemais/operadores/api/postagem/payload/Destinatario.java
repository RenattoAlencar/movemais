package movemais.operadores.api.postagem.payload;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder=true)
public class Destinatario implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@ApiModelProperty("Devires")
	@NotBlank
	private String nome;
	
	@ApiModelProperty("15046546414")
	@NotBlank
	private String documento;
	
	@ApiModelProperty("destinatario@mailinator.com")
	private String email;
	
	@Valid
	@NotNull
	private Telefone telefone;
	
	@Valid
	@NotNull
	private Endereco endereço;

}
