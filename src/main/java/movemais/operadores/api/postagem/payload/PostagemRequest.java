package movemais.operadores.api.postagem.payload;

import java.io.Serializable;
import java.util.Map;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;
import movemais.operadores.api.pedidos.model.Transportadora;

@With
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder=true)
public class PostagemRequest implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(hidden = true)
	private UUID id;
	
	@Valid
	@NotNull
	private Destinatario destinatario;
	
	@ApiModelProperty("TESTSE")
	@NotNull
	private Transportadora transportadora;
	
	
	@NotNull
	private Map<String, String> dadosAdicionais;

}
