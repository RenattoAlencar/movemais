package movemais.operadores.api.postagem.payload;

import java.util.Map;
import java.util.UUID;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder=true)
public class PostagemResponse {

	private UUID id;
	
	
	private Destinatario destinatario;
	
	@ApiModelProperty("JADLOG")
	private String transportadora;

	@ApiModelProperty(notes = "Dados Adicionais ", position = 1)
	private Map<String, String> dadosAdicionais;
	
}
