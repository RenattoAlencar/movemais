package movemais.operadores.api.postagem.payload;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder=true)
public class Telefone implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@ApiModelProperty("14")
	@NotBlank
	private String ddd;
	
	@ApiModelProperty("46546546")
	@NotBlank
	private String numero;

}
