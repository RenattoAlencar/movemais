package movemais.operadores.api.postagem.payload;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder=true)
public class Endereco implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@ApiModelProperty("11040101")
	@NotBlank
	@Size(max = 8, min = 8)
	private String cep;
	
	@ApiModelProperty("Rua 38")
	@NotBlank
	private String rua;
	
	@ApiModelProperty("Vila São José")
	@NotBlank
	private String bairro;
	
	@ApiModelProperty("Jau")
	@NotBlank
	private String cidade;
	
	@ApiModelProperty("SP")
	@NotBlank
	private String estado;
	
	@ApiModelProperty("475")
	private String numero;
	
	@ApiModelProperty("Ap 34")
	private String complemento;

}
