package movemais.operadores.api.postagem.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import movemais.operadores.api.postagem.entities.PostagemEntity;

@Repository
public interface PostagemRepository extends JpaRepository<PostagemEntity, UUID> {

}