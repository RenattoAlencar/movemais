package movemais.operadores.api.postagem.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name="postagem_item")
public class PostagemItem implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PostagemItemPk pk;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id", referencedColumnName = "id", insertable=false, updatable=false, nullable = false)
	private PostagemEntity postagem;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "created_at", nullable = false, updatable = false, columnDefinition = "TIMESTAMP")
	private Date createdAt;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "updated_at", nullable = true, columnDefinition = "TIMESTAMP")
	private Date updatedAt; 

}
