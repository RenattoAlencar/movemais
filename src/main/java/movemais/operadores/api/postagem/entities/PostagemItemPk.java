package movemais.operadores.api.postagem.entities;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class PostagemItemPk implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Column(name = "id", nullable = false)
	private UUID id;
	
	@Column(name = "chave", nullable = false)
	private String chave; 
	
	@Column(name = "valor", nullable = false)
	private String valor; 

}
