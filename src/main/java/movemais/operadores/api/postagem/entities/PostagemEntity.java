package movemais.operadores.api.postagem.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import movemais.operadores.api.pedidos.model.Transportadora;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name="postagem")
public class PostagemEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id", nullable = false)
	private UUID id;
	
	@Column(name = "id_externo", nullable = false)
	private String idExterno;
	
	@Column(name = "nome_destinatario", nullable = false)
	private String nomeDestinatario;
	
	@Column(name = "documento_destinatario", nullable = false)
	private String documentoDestinatario;
	
	@Column(name = "email_destinatario", nullable = false)
	private String emailDestinatario;
	
	@Column(name = "ddd_destinatario", nullable = false)
	private String dddDestinatario;
	
	@Column(name = "telefone_destinatario", nullable = false)
	private String telefoneDestinatario;
	
	@Column(name = "cep_destinatario", nullable = false)
	private String cepDestinatario;
	
	@Column(name = "rua_destinatario", nullable = false)
	private String ruaDestinatario;
	
	@Column(name = "bairro_destinatario", nullable = false)
	private String bairroDestinatario;
	
	@Column(name = "cidade_destinatario", nullable = false)
	private String cidadeDestinatario;
	
	@Column(name = "estado_destinatario", nullable = false)
	private String estadoDestinatario;
	
	@Column(name = "numero_destinatario", nullable = false)
	private String numeroDestinatario;
	
	@Column(name = "complemento_destinatario", nullable = false)
	private String complementoDestinatario;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "postagem", cascade = CascadeType.ALL)
	private List<PostagemItem> postagemItemList;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "transportadora", nullable = false)
	private Transportadora transportadora;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "created_at", nullable = false, updatable = false, columnDefinition = "TIMESTAMP")
	private Date createdAt;
	
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "updated_at", nullable = true, columnDefinition = "TIMESTAMP")
	private Date updatedAt;
	
	
	public PostagemEntity addPostagemItem(UUID id, String chave, String valor) {
		
		PostagemItemPk pk = PostagemItemPk.builder()
				.id(id)
				.chave(chave)
				.valor(valor)
				.build();
		
		PostagemItem item = PostagemItem.builder()
				.pk(pk)
				.createdAt(new Date())
				.build();
		
		if(this.postagemItemList == null) {
			this.postagemItemList = new ArrayList<PostagemItem>();
		}
		
		this.postagemItemList.add(item);
		
		return this;
	}
}
