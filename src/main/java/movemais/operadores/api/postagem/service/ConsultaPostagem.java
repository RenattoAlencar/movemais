package movemais.operadores.api.postagem.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import movemais.operadores.api.postagem.entities.PostagemEntity;
import movemais.operadores.api.postagem.repository.PostagemRepository;

@Service
@Transactional(readOnly = true)
public class ConsultaPostagem {
	
	@Autowired
	private PostagemRepository repository; 
	
	
	public PostagemEntity findById(String id) {
		
		return repository.getOne(UUID.fromString(id));
		
	}

}
