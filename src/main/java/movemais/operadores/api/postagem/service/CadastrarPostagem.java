package movemais.operadores.api.postagem.service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import movemais.operadores.api.jadlog.payload.PedidoJadLogResponse;
import movemais.operadores.api.postagem.entities.PostagemEntity;
import movemais.operadores.api.postagem.entities.PostagemItem;
import movemais.operadores.api.postagem.entities.PostagemItemPk;
import movemais.operadores.api.postagem.payload.Endereco;
import movemais.operadores.api.postagem.payload.PostagemRequest;
import movemais.operadores.api.postagem.payload.Telefone;
import movemais.operadores.api.postagem.repository.PostagemRepository;
import movemais.operadores.api.translevelog.payload.PedidoTransLeveLogResponse;

@Service
@Transactional
public class CadastrarPostagem {
	
	@Autowired
	private PostagemRepository repository;
	

	public PostagemEntity run(PostagemRequest input, PedidoTransLeveLogResponse out) {

		Telefone telefone = input.getDestinatario().getTelefone();
		Endereco endereco = input.getDestinatario().getEndereço();
		
		PostagemEntity entity = PostagemEntity.builder()
				.id(input.getId())
				.idExterno(out.getOrderId())
				.transportadora(input.getTransportadora())
				.nomeDestinatario(input.getDestinatario().getNome())
				.documentoDestinatario(input.getDestinatario().getDocumento())
				.emailDestinatario(input.getDestinatario().getEmail())
				.dddDestinatario(telefone.getDdd())
				.telefoneDestinatario(telefone.getNumero())
				.cepDestinatario(endereco.getCep())
				.ruaDestinatario(endereco.getRua())
				.bairroDestinatario(endereco.getBairro())
				.cidadeDestinatario(endereco.getCidade())
				.estadoDestinatario(endereco.getEstado())
				.numeroDestinatario(endereco.getNumero())
				.complementoDestinatario(endereco.getComplemento())
				.createdAt(new Date())
				.build();
		
		if(MapUtils.isNotEmpty(input.getDadosAdicionais())) {
			List<PostagemItem> postagemItemList = input.getDadosAdicionais().entrySet().stream().map(mapper -> {
				PostagemItemPk pk = PostagemItemPk.builder()
						.id(entity.getId())
						.chave(mapper.getKey())
						.valor(mapper.getValue())
						.build();
				
				return PostagemItem.builder()
						.pk(pk)
						.createdAt(new Date())
						.build();
			}).collect(Collectors.toList());
			
			entity.setPostagemItemList(postagemItemList);
		}
		
		return repository.save(entity);
	}


	public PostagemEntity run(PostagemRequest input, PedidoJadLogResponse out) {
		
		Telefone telefone = input.getDestinatario().getTelefone();
		Endereco endereco = input.getDestinatario().getEndereço();
		
		PostagemEntity entity = PostagemEntity.builder()
				.id(input.getId())
				.idExterno(out.getCodigo())
				.transportadora(input.getTransportadora())
				.nomeDestinatario(input.getDestinatario().getNome())
				.documentoDestinatario(input.getDestinatario().getDocumento())
				.emailDestinatario(input.getDestinatario().getEmail())
				.dddDestinatario(telefone.getDdd())
				.telefoneDestinatario(telefone.getNumero())
				.cepDestinatario(endereco.getCep())
				.ruaDestinatario(endereco.getRua())
				.bairroDestinatario(endereco.getBairro())
				.cidadeDestinatario(endereco.getCidade())
				.estadoDestinatario(endereco.getEstado())
				.numeroDestinatario(endereco.getNumero())
				.complementoDestinatario(endereco.getComplemento())
				.createdAt(new Date())
				.build();
		
		if(MapUtils.isNotEmpty(input.getDadosAdicionais())) {
			input.getDadosAdicionais().entrySet().stream().forEach(action -> {
				entity.addPostagemItem(entity.getId(), action.getKey(), action.getValue());
			});
		}
		
		return repository.save(entity);
	}
	
}