package movemais.operadores.api.postagem.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import movemais.operadores.api.jadlog.integracao.exceptions.JadLogException;
import movemais.operadores.api.jadlog.payload.PedidoJadLogResponse;
import movemais.operadores.api.jadlog.service.JadLogPedidoService;
import movemais.operadores.api.postagem.entities.PostagemEntity;
import movemais.operadores.api.postagem.payload.PostagemRequest;
import movemais.operadores.api.postagem.payload.PostagemResponse;
import movemais.operadores.api.translevelog.payload.PedidoTransLeveLogResponse;
import movemais.operadores.api.translevelog.service.TransLeveLogPedidoService;

@Service
@Transactional
public class PostagemService {
	
	@Autowired
	private ValidadorPostagem validarPostagem;
	
	@Autowired
	private CadastrarPostagem cadastrarPostagem;

	@Autowired
	private TransLeveLogPedidoService translevelog;
	
	@Autowired
	private JadLogPedidoService jadlog;
	
	
	public PostagemResponse run(PostagemRequest input) throws JadLogException {
		
		//validacao do request
		validarPostagem.run(input);

		//cria um novo id
		input = input.withId(UUID.randomUUID());
		
		PostagemResponse output = null;
		
		//chevear p/ por transportadora 
		switch (input.getTransportadora()) {
		case CORREIOS:
			throw new RuntimeException("Not implemented");

		case TRANSLEVELOG: {
			
			PedidoTransLeveLogResponse out = translevelog.run(input);
			
			PostagemEntity entity = cadastrarPostagem.run(input, out);
			
			output = PostagemResponse.builder()
					.id(entity.getId())
					.build();
			
		} break;
		case JADLOG: {
			
			PedidoJadLogResponse out = jadlog.run(input);
			
			PostagemEntity entity = cadastrarPostagem.run(input, out);
			
			output = PostagemResponse.builder()
					.id(entity.getId())
					.build();

		} break;
		default:
			throw new RuntimeException("Transportadora não cadastrada.");
		}
		
		
		return output;
	}

}
