package movemais.operadores.api.postagem.endpoint;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import movemais.operadores.api.jadlog.integracao.exceptions.JadLogException;
import movemais.operadores.api.pedidos.model.Etiqueta;
import movemais.operadores.api.pedidos.model.Transportadora;
import movemais.operadores.api.postagem.entities.PostagemEntity;
import movemais.operadores.api.postagem.entities.PostagemItem;
import movemais.operadores.api.postagem.payload.Destinatario;
import movemais.operadores.api.postagem.payload.Endereco;
import movemais.operadores.api.postagem.payload.PostagemRequest;
import movemais.operadores.api.postagem.payload.PostagemResponse;
import movemais.operadores.api.postagem.payload.Telefone;
import movemais.operadores.api.postagem.service.ConsultaPostagem;
import movemais.operadores.api.postagem.service.PostagemService;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Api(tags = "Postagem")
@RestController
@RequestMapping("postagens")
public class PostagensEndpoint {

	@Autowired
	private ConsultaPostagem consultaPostagem;

	@Autowired
	private PostagemService postagemService;

	@ApiOperation(value = "Buscar Postagem por Id ", notes = "Informar o Id para consultar a postagem ")
	@GetMapping("/{id}")
	public ResponseEntity<PostagemResponse> get(@PathVariable("id") String id) {

		PostagemEntity entity = consultaPostagem.findById(id);

		Endereco endereco = Endereco.builder().bairro(entity.getBairroDestinatario()).cep(entity.getCepDestinatario())
				.cidade(entity.getCidadeDestinatario()).complemento(entity.getComplementoDestinatario())
				.estado(entity.getEstadoDestinatario()).numero(entity.getNumeroDestinatario())
				.rua(entity.getRuaDestinatario()).build();

		Telefone telefone = Telefone.builder().ddd(entity.getDddDestinatario()).numero(entity.getNumeroDestinatario())
				.build();

		Destinatario destinatario = Destinatario.builder().documento(entity.getDocumentoDestinatario())
				.email(entity.getEmailDestinatario()).nome(entity.getNomeDestinatario()).endereço(endereco)
				.telefone(telefone).build();

		Map<String, String> dadosAdicionais = entity.getPostagemItemList().stream().collect(Collectors
				.toMap(keyMapper -> keyMapper.getPk().getChave(), valueMapper -> valueMapper.getPk().getValor()));

		PostagemResponse output = PostagemResponse.builder().id(entity.getId()).destinatario(destinatario)
				.transportadora(entity.getTransportadora().name()).dadosAdicionais(dadosAdicionais).build();

		return ResponseEntity.ok(output);
	}

	@ApiOperation(value = "Enviar Postagem dos Operadores ( JADLOG ou TRANSLEVELOG )", notes = "DADOS ADICIONAIS: Ira Receber informações de cada transportadora")
	@PostMapping
	public ResponseEntity<PostagemResponse> post(@RequestBody @Valid PostagemRequest input,
			UriComponentsBuilder builder) throws JadLogException {
		PostagemResponse output = postagemService.run(input);
		URI uri = builder.path("/postagens/{id}").buildAndExpand(output.getId().toString()).toUri();
		return ResponseEntity.created(uri).body(output);
	}

	@ApiOperation(value = "Emitir Etiqueta", notes = "Passar Id do Operador para Emitir a Etiqueta")
	@GetMapping("/{id}/etiquetas")
	public ResponseEntity<byte[]> getEtiqueta(@PathVariable("id") String id) throws IOException, JRException {

		String jasperReport = "";

		PostagemEntity entity = consultaPostagem.findById(id);

		String telefone = StringUtils.join(entity.getDddDestinatario(), entity.getTelefoneDestinatario());

		Etiqueta etiqueta = new Etiqueta();
		etiqueta.setNome(entity.getNomeDestinatario());
		etiqueta.setCpfCnpj(entity.getDocumentoDestinatario());
		etiqueta.setEndereco(entity.getRuaDestinatario());
		etiqueta.setBairro(entity.getBairroDestinatario());
		etiqueta.setEstado(entity.getEstadoDestinatario());
		etiqueta.setNumero(entity.getNumeroDestinatario());
		etiqueta.setCidade(entity.getCidadeDestinatario());
		etiqueta.setCep(entity.getCepDestinatario());
		etiqueta.setTelefone(entity.getTelefoneDestinatario());
		etiqueta.setCelular(telefone);
		etiqueta.setEmail(entity.getEmailDestinatario());
		
		jasperReport = "arquivo\\Etiqueta.jasper";

		if (Transportadora.JADLOG.equals(entity.getTransportadora())) {
			Optional<PostagemItem> Op = entity.getPostagemItemList().stream()
					.filter(predicate -> predicate.getPk().getChave().equals("contato")).findFirst();
			if (Op.isPresent()) {
				etiqueta.setContato(Op.get().getPk().getValor());

			}
		}

		JRDataSource dataSource = new JRBeanCollectionDataSource(Arrays.asList(etiqueta));
		JasperPrint report = JasperFillManager.fillReport(jasperReport, new HashMap<String, Object>(), dataSource);
		byte[] contents = JasperExportManager.exportReportToPdf(report);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_PDF);
		String filename = "output.pdf";
		headers.setContentDispositionFormData(filename, filename);
		headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		ResponseEntity<byte[]> response = new ResponseEntity<>(contents, headers, HttpStatus.OK);
		return response;

	}

}
