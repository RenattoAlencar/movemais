package movemais.operadores.api.translevelog.service;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import movemais.operadores.api.translevelog.integracao.IntegracaoTransLeveLogCliente;
import movemais.operadores.api.translevelog.payload.ConsultaTransLeveLogRequest;
import movemais.operadores.api.translevelog.payload.ConsultaTransLeveLogResponse;
import movemais.operadores.api.translevelog.payload.integracao.History;
import movemais.operadores.api.translevelog.payload.integracao.TransLeveLogConsultaRequest;
import movemais.operadores.api.translevelog.payload.integracao.TransLeveLogConsultaResponse;

@Service
public class TransLeveLogConsultaService {

	@Autowired
	private IntegracaoTransLeveLogCliente integracaoTransLeveLogCliente;
	
	
	public ConsultaTransLeveLogResponse translevel(ConsultaTransLeveLogRequest input) {
		TransLeveLogConsultaRequest request = transform(input);
		TransLeveLogConsultaResponse response = integracaoTransLeveLogCliente.search(request);
		ConsultaTransLeveLogResponse output = transform(response);
		return output;
	}
	
	private TransLeveLogConsultaRequest transform(ConsultaTransLeveLogRequest input) {
		return TransLeveLogConsultaRequest.builder()
				.orderId(input.getOrderId())
				.onlyLast(input.getOnlyLast())
				.build();
	}

	private ConsultaTransLeveLogResponse transform(TransLeveLogConsultaResponse response) {
		return ConsultaTransLeveLogResponse.builder()
				.barCode(response.getBarCode())
				.history(Arrays.asList(History.builder()
						.statusDesciption(response.getHistory().stream().findFirst().get().getStatusDesciption())
						.date(response.getHistory().stream().findFirst().get().getDate())
						.statusCode(response.getHistory().stream().findFirst().get().getStatusCode())
						.build()))
				.orderId(response.getOrderId())
				.build();
				
	}

}