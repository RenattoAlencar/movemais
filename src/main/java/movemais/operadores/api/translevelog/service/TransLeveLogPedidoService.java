package movemais.operadores.api.translevelog.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import movemais.operadores.api.postagem.payload.Endereco;
import movemais.operadores.api.postagem.payload.PostagemRequest;
import movemais.operadores.api.postagem.payload.Telefone;
import movemais.operadores.api.translevelog.integracao.IntegracaoTransLeveLogCliente;
import movemais.operadores.api.translevelog.model.TransLeveLogModel;
import movemais.operadores.api.translevelog.payload.PedidoTransLeveLogRequest;
import movemais.operadores.api.translevelog.payload.PedidoTransLeveLogResponse;
import movemais.operadores.api.translevelog.payload.integracao.TransLeveLogPedidoRequest;
import movemais.operadores.api.translevelog.payload.integracao.TransLeveLogPedidoResponse;
import movemais.operadores.api.translevelog.repository.TransLeveLogRepository;
import movemais.operadores.api.utils.NumerosUtils;

@Service
public class TransLeveLogPedidoService {

	@Autowired
	private IntegracaoTransLeveLogCliente integracaoTransLeveLogCliente;
	
	@Autowired
	private TransLeveLogRepository transLeveLogRepository;
	
	
	public PedidoTransLeveLogResponse run(PostagemRequest input) {
		TransLeveLogPedidoRequest request = transform(input);
		TransLeveLogPedidoResponse response = integracaoTransLeveLogCliente.run(request);
		return transform(response);
	}
	
	private TransLeveLogPedidoRequest transform(PostagemRequest input) {
		
		Telefone telefone = input.getDestinatario().getTelefone();
		Endereco endereco = input.getDestinatario().getEndereço();
		
		String numeroTelefone = null;
		
		if(telefone != null) {
			numeroTelefone = StringUtils.join(input.getDestinatario().getTelefone().getDdd(), input.getDestinatario().getTelefone().getNumero()); 
		}
		
		return TransLeveLogPedidoRequest.builder()
				.partnerOrderNumber(input.getId().toString())
				.personName(input.getDestinatario().getNome())
				.personDocument(input.getDestinatario().getDocumento())
				.phoneNumber(numeroTelefone)
				.postalCode(endereco.getCep())
				.street(endereco.getRua())
				.streetNumber(endereco.getNumero())
				.complement(endereco.getComplemento())
				.neighborhood(endereco.getBairro())
				.city(endereco.getCidade())
				.state(endereco.getEstado())
				.barcode(input.getDadosAdicionais().get("barcode"))
				.height(NumerosUtils.parseDouble(input.getDadosAdicionais().get("height")))
				.width(NumerosUtils.parseDouble(input.getDadosAdicionais().get("width")))
				.length(NumerosUtils.parseDouble(input.getDadosAdicionais().get("length")))
				.weight(NumerosUtils.parseDouble(input.getDadosAdicionais().get("weight")))
				.invoiceNumber(input.getDadosAdicionais().get("invoiceNumber"))
				.build();
	}

	
	public PedidoTransLeveLogResponse translevelog(PedidoTransLeveLogRequest input) {
		TransLeveLogPedidoRequest request = format(input);
		TransLeveLogPedidoResponse response = integracaoTransLeveLogCliente.run(request);
		PedidoTransLeveLogResponse output = transform(response);
		cadastrobd(input, response);
		return output;
	}
	
	private TransLeveLogModel cadastrobd(PedidoTransLeveLogRequest request, TransLeveLogPedidoResponse response) {
		TransLeveLogModel translevel = TransLeveLogModel.builder()
				.barcode(request.getBarcode())
				.partnerOrderNumber(request.getPartnerOrderNumber())
				.postalCode(request.getPostalCode())
				.street(request.getStreet())
				.streetNumber(request.getStreetNumber())
				.complement(request.getComplement())
				.neighborhood(request.getNeighborhood())
				.city(request.getCity())
				.state(request.getState())
				.personName(request.getPersonName())
				.personDocument(request.getPersonDocument())
				.phoneNumber(request.getPhoneNumber())
				.height(request.getHeight())
				.width(request.getWidth())
				.length(request.getLength())
				.weight(request.getWeight())
				.invoiceNumber(request.getInvoiceNumber())
				.orderId(response.getOrderId())
				.build();
		return transLeveLogRepository.save(translevel);
	}
	
	private TransLeveLogPedidoRequest format(PedidoTransLeveLogRequest input) {
		return TransLeveLogPedidoRequest.builder()
				.barcode(input.getBarcode())
				.partnerOrderNumber(input.getPartnerOrderNumber())
				.postalCode(input.getPostalCode())
				.street(input.getStreet())
				.streetNumber(input.getStreetNumber())
				.complement(input.getComplement())
				.neighborhood(input.getNeighborhood())
				.city(input.getCity())
				.state(input.getState())
				.personName(input.getPersonName())
				.personDocument(input.getPersonDocument())
				.phoneNumber(input.getPhoneNumber())
				.height(input.getHeight())
				.width(input.getWidth())
				.length(input.getLength())
				.weight(input.getWeight())
				.invoiceNumber(input.getInvoiceNumber())
				.build();
	}

	private PedidoTransLeveLogResponse transform(TransLeveLogPedidoResponse response) {
		return PedidoTransLeveLogResponse.builder()
				.orderId(String.valueOf(response.getOrderId()))
				.build();
	}

}
