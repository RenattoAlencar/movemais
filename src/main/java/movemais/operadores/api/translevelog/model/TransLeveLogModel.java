package movemais.operadores.api.translevelog.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Entity
@Table(name = "TB_TRANSLEVELOG")
public class TransLeveLogModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "barcode", nullable = false)
	private String barcode;

	@Column(name = "partnerOrderNumber", nullable = false)
	private String partnerOrderNumber;

	@Column(name = "postalCode", nullable = false)
	private String postalCode;

	@Column(name = "street", nullable = false)
	private String street;

	@Column(name = "streetNumber", nullable = false)
	private String streetNumber;

	@Column(name = "complement", nullable = false)
	private String complement;

	@Column(name = "neighborhood", nullable = false)
	private String neighborhood;

	@Column(name = "city", nullable = false)
	private String city;

	@Column(name = "state", nullable = false)
	private String state;

	@Column(name = "personName", nullable = false)
	private String personName;

	@Column(name = "personDocument", nullable = false)
	private String personDocument;

	@Column(name = "phoneNumber", nullable = false)
	private String phoneNumber;

	@Column(name = "height", nullable = false)
	private Double height;

	@Column(name = "width", nullable = false)
	private Double width;

	@Column(name = "length", nullable = false)
	private Double length;

	@Column(name = "weight", nullable = false)
	private Double weight;

	@Column(name = "invoiceNumber", nullable = false)
	private String invoiceNumber;

	@Column(name = "orderId", nullable = false)
	private Integer orderId;

//	@OneToOne(mappedBy = "translevelog")
//	@JoinColumn(name = "pedidos_id")
//	private Pedidos pedidos;
}
