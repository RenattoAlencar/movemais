package movemais.operadores.api.translevelog.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import movemais.operadores.api.translevelog.model.TransLeveLogModel;

public interface TransLeveLogRepository extends JpaRepository<TransLeveLogModel, Long> {

}
