package movemais.operadores.api.translevelog.integracao;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;
import movemais.operadores.api.translevelog.payload.integracao.TransLeveLogConsultaRequest;
import movemais.operadores.api.translevelog.payload.integracao.TransLeveLogConsultaResponse;
import movemais.operadores.api.translevelog.payload.integracao.TransLeveLogPedidoRequest;
import movemais.operadores.api.translevelog.payload.integracao.TransLeveLogPedidoResponse;

@Slf4j
@Component
public class IntegracaoTransLeveLogCliente {

	@Value("${movemais.operadores.api.translevellog.integracao.token}")
	private String token;

	@Value("${movemais.operadores.api.translevellog.integracao.usercode}")
	private Integer usercode;
	
	@Value("${movemais.operadores.api.translevellog.integracao.endpoint}")
	private String ENDPOINT;
	
	@Value("${movemais.operadores.api.translevellog.integracao.endpointconsulta}")
	private String ENDPOINTSEARCH;

	private RestTemplate restTemplate;


	@PostConstruct
	public void init() {
		restTemplate = new RestTemplateBuilder().build();
	}

	public TransLeveLogPedidoResponse run(TransLeveLogPedidoRequest input) {
		input = input.toBuilder().token(token).userCode(usercode).build();
		try {
			String path = String.format("%s/OrderApi", ENDPOINT);
			HttpEntity<TransLeveLogPedidoRequest> request = new HttpEntity<TransLeveLogPedidoRequest>(input);
			ResponseEntity<TransLeveLogPedidoResponse> response = restTemplate.postForEntity(path, request,
					TransLeveLogPedidoResponse.class);
			return response.getBody();

		} catch (HttpClientErrorException error) {
			log.error("ERROR: {}", error.getResponseBodyAsString());
			throw error;
		} catch (HttpServerErrorException error) {
			log.error("ERROR: {}", error.getResponseBodyAsString());
			throw error;
		}
	}

	public TransLeveLogConsultaResponse search(TransLeveLogConsultaRequest input) {
		input = input.toBuilder()
				.token(token)
				.userCode(usercode)
				.build();
		String path = String.format("%s/OrderTracking", ENDPOINTSEARCH);
		HttpEntity<TransLeveLogConsultaRequest> request = new HttpEntity<TransLeveLogConsultaRequest>(input);
		ResponseEntity<TransLeveLogConsultaResponse> response = restTemplate.postForEntity(path, request, TransLeveLogConsultaResponse.class);
		return response.getBody();
		
	}
}
