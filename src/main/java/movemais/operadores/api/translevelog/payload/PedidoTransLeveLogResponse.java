package movemais.operadores.api.translevelog.payload;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class PedidoTransLeveLogResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty("2694")
	private String orderId;
	
}