package movemais.operadores.api.translevelog.payload.integracao;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAlias;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class TransLeveLogConsultaResponse implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@JsonAlias("Barcode")
	private String barCode;
	
	@JsonAlias("History")
	private List<History> history;
	
	@JsonAlias("OrderId")
	private Integer orderId;
	
	
}
