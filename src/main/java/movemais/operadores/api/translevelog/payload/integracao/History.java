package movemais.operadores.api.translevelog.payload.integracao;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class History implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@JsonAlias(value = "StatusDescription")
	private String statusDesciption;
	
	@JsonAlias(value = "Date")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date date;
	
	@JsonAlias(value = "StatusCode")
	private String statusCode;
	
}
