package movemais.operadores.api.translevelog.payload.integracao;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class TransLeveLogPedidoRequest implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@JsonAlias(value = "Token")
	private String token;
	
	@JsonAlias("UserCode")
	private Integer userCode;
	
	@JsonAlias(value = "Barcode")
	private String barcode;

	@JsonAlias(value = "PartnerOrderNumber")
	private String partnerOrderNumber;
	
	@JsonAlias(value = "PostalCode")
	private String postalCode;
	
	@JsonAlias(value = "Street")
	private String street;
	
	@JsonAlias(value = "StreetNumber")
	private String streetNumber;
	
	@JsonAlias(value = "Complement")
	private String complement;
	
	@JsonAlias(value = "Neighborhood")
	private String neighborhood;
	
	@JsonAlias(value = "City")
	private String city;
	
	@JsonAlias(value = "State")
	private String state;
	
	@JsonAlias(value = "PersonName")
	private String personName;
	
	@JsonAlias(value = "PersonDocument")
	private String personDocument;
	
	@JsonAlias(value = "PhoneNumber")
	private String phoneNumber;
	
	@JsonAlias(value = "Height")
	private Double height;
	
	@JsonAlias(value = "Width")
	private Double width;
	
	@JsonAlias(value = "Length")
	private Double length;
	
	@JsonAlias(value = "Weight")
	private Double weight;
	
	@JsonAlias(value = "InvoiceNumber")
	private String invoiceNumber;
	
	
}
