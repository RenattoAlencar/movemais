package movemais.operadores.api.translevelog.payload.integracao;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAlias;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class TransLeveLogConsultaRequest implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@JsonAlias(value = "Token")
	private String token;
	
	@JsonAlias(value = "UserCode")
	private Integer userCode;
	
	@JsonAlias(value = "orderId")
	private Integer orderId;
	
	@JsonAlias(value = "onlyLast")
	private Boolean onlyLast;
	
}
