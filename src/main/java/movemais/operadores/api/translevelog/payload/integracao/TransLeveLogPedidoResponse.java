package movemais.operadores.api.translevelog.payload.integracao;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class TransLeveLogPedidoResponse implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Integer orderId;
	
}
