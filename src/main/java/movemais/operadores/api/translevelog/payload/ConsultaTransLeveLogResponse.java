package movemais.operadores.api.translevelog.payload;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;
import movemais.operadores.api.translevelog.payload.integracao.History;


@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class ConsultaTransLeveLogResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty("0122344562723840010056")
	private String barCode;
	private List<History> history;
	
	@ApiModelProperty("2694")
	private Integer orderId;
	
	
}
