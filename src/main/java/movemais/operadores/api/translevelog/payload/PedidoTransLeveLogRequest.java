package movemais.operadores.api.translevelog.payload;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class PedidoTransLeveLogRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty("0122344562723840010056")
	@NotNull @NotEmpty
	private String barcode;
	
	@ApiModelProperty("23010")
	@NotNull @NotEmpty
	private String partnerOrderNumber;
	
	@ApiModelProperty("06223130")
	@NotNull @NotEmpty
	private String postalCode;
	
	@ApiModelProperty("Av: Paulista")
	@NotNull @NotEmpty
	private String street;
	
	@ApiModelProperty("298")
	@NotNull @NotEmpty
	private String streetNumber;
	
	@ApiModelProperty("Casa 1")
	@NotNull @NotEmpty
	private String complement;
	
	@ApiModelProperty("Rochdale")
	@NotNull @NotEmpty
	private String neighborhood;
	
	@ApiModelProperty("Osasco")
	@NotNull @NotEmpty
	private String city;
	
	@ApiModelProperty("SP")
	@NotNull @NotEmpty
	private String state;
	
	@ApiModelProperty("Ze Maria")
	@NotNull @NotEmpty
	private String personName;
	
	@ApiModelProperty("471627384")
	@NotNull @NotEmpty
	private String personDocument;
	
	@ApiModelProperty("11999999999")
	@NotNull @NotEmpty
	private String phoneNumber;
	
	@ApiModelProperty("1")
	@NotNull 
	private Double height;
	
	@ApiModelProperty("2")
	@NotNull 
	private Double width;
	
	@ApiModelProperty("5")
	@NotNull 
	private Double length;
	
	@ApiModelProperty("10")
	@NotNull 
	private Double weight;
	
	@ApiModelProperty("Ze Maria")
	@NotNull 
	private String invoiceNumber;

}
