package movemais.operadores.api.utils;

public class NumerosUtils {
	
	
	public static Double parseDouble(String input) {
		
		if(input == null) {
			return null;
		}
		
		try {
			return Double.parseDouble(input);
		} catch (Exception e) {
			return null;
		}
		
	}
	
	public static Integer parseInt(String input) {
		
		if(input == null) {
			return null;
		}
		
		try {
			return Integer.parseInt(input);
		} catch (Exception e) {
			return null;
		}
		
	}

}
